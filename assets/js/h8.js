$(document).ready(function() {
	$(".various").fancybox({

		maxWidth	: 800,
		padding		:0,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize    : true,
        autoScale   : true,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
        overlay : {
            css : {
                'background' : 'rgba(255, 255, 255, 0.2)'
            }
        }
    } 

	});
});